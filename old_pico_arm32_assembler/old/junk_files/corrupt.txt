           nit: define :: ;

.	define unit: define-list :: :: :: :: end-list;
.	define unit: define-list-8 :: :: :: :: :: :: :: :: end-list;
.	define unit: end of file ;
.	define unit: comment :: end;

.	define unit: // :: ;

.define-list-8
	unit:nop;
	unit:yield;
	unit:wfe;
	unit:wfi;

	unit:sev;
	unit:adc:r:,:r:,:r:;
	unit:emit :imm: ;
	unit:emit word :imm: ;
end-list

.define 	unit:adds:r:,:r:,:r:;


.define-list-8
	unit:add:R:,:R:;
	unit:subs:r:,:r:,:r:;
	unit:cmp:r:,:r:;
	unit:cmn:r:,:r:;

	unit:cmp:r:,:imm:;
	unit:muls:r:,:r:;
	unit:movs:r:,:imm:;
	unit:movs:r:,:r:;
end-list

.define-list-8
	unit:mov:R:,:R:;
	unit:mvns:r:,:r:;
	unit:lsls:r:,:r:,:imm:;
	unit:lsls:r:,:r:;
	
	unit:lsrs:r:,:r:,:imm:;
	unit:lsrs:r:,:r:;
	unit:asrs:r:,:r:,:imm:;
	unit:asrs:r:,:r:;
end-list

.define-list-8
	unit:bics:r:,:r:;
	unit:tst:r:,:r:;
	unit:ands:r:,:r:;
	unit:eors:r:,:r:;
	
	unit:ldr:r:,[:r:,:r:];
	unit:ldr:r:,[:r:,:imm:];
	unit:ldr:r:,[sp,:imm:];
	unit:ldrc:r:,[pc,:imm:];
end-list

.define-list

	unit:orrs:r:,:r:;
	unit:str:r:,[:r:,:r:];
	unit:str:r:,[:r:,:imm:];
	unit:str:r:,[sp,:imm:];

end-list


.	define 	unit:ldr:r:,:label:;
.	define unit: b :label: ;
.	define unit: b :cond: :label: ;

.	define-list-8 imm:c0; imm:c1; imm:c2; imm:c3; imm:c4; imm:c5; imm:c6; imm:c7; end-list
.	define-list-8 imm:c8; imm:c9; imm:c10; imm:c11; imm:c12; imm:c13; imm:c14; imm:c15; end-list
.	define-list-8 imm:c16; imm:c17; imm:c18; imm:c19; imm:c20; imm:c21; imm:c22; imm:c23; end-list
.	define-list-8 imm:c24; imm:c25; imm:c26; imm:c27; imm:c28; imm:c29; imm:c30; imm:c31; end-list

. 	define-list-8 r:r0; r:r1; r:r2; r:r3; r:r4; r:r5; r:r6; r:r7; end-list

. 	define-list-8 R:r0; R:r1; R:r2; R:r3; R:r4; R:r5; R:r6; R:r7; end-list
. 	define-list-8 R:r8; R:r9; R:r10; R:r11; R:r12; R:r13; R:r14; R:r15; end-list

. 	define-list cond:eq; cond:ne; cond:cs; cond:cc; end-list
. 	define-list cond:mi; cond:pl; cond:vs; cond:vc; end-list
. 	define-list cond:hi; cond:ls; cond:ge; cond:lt; end-list
. 	define-list cond:gt; cond:le; cond:al; cond:unused_cond_15; end-list

. define-list-8

	unit: :label: \: :unit: ;
	unit: at :ctl: \: :unit: ;

	unit:address:imm:;
	unit:increment:imm:;

	unit:decrement:imm:;
	unit:zero:imm:;
	unit:complement:imm:,:imm:;
	unit:copy:imm:,:imm:;
end-list

. define-list-8
	unit:add:imm:,:imm:,:imm:;
	unit:subtract:imm:,:imm:,:imm:;
	unit:multiply:imm:,:imm:,:imm:;
	unit:xor:imm:,:imm:,:imm:;

	unit:and:imm:,:imm:,:imm:;
	unit:or:imm:,:imm:,:imm:;
	unit:right:imm:,:imm:,:imm:;
	unit:left:imm:,:imm:,:imm:;
end-list

. define-list-8

 	unit:store[:imm:],:imm:;
 	unit:load:imm:,[:imm:];

	unit:signed if :imm: is less than :imm:,:ctl:;
	unit:if :imm: is less than :imm:,:ctl:;


end-list


.comment -------------------------------- user code: ------------------------------------ ;end

.	define ctl: keep looping;

. 	nop
. 	yield
.	wfe
.	wfi
.	sev

.	decrement c2
.	emit c2

.	increment c1
.	emit word c1
.	zero c1

.	increment c1
.	add c1, c1, c1
.	add c1, c1, c1
.	increment c1
.	add c1, c1, c1


.at keep looping: 
	
	address c2
.	emit c2
.	increment c0

.	if c0 is less than c1, keep looping

.	increment c5
.	emit word c5



end of file







