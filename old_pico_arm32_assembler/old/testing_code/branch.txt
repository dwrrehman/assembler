.// 	  
///	this is a testing file, which will be used to test 
///      whether all ARM instructions byte generation work, 
///           and emit the right bytes.
///
///      written by daniel rehman, on 2104283.153305.
//;

.	define label: my label;



.	nop
.	nop
.	nop
.	nop
.	nop

.my label: 
	sev




.	nop
.	nop
.	nop
.	nop
.	nop
.	cmp r3, r4
.	bne my label

.	nop
.	nop
.	nop
.	nop
.	nop


.comment . 	ldr r2, my label    ;end-comment






end of file
