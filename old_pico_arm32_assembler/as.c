//
//    An assembler for the Cortex-M0 Plus.
//    specifcally made for use in programming 
//    the RP2040, found in the Raspberry Pi Pico.
// 
//    Made by Daniel Warren Riaz Rehman,
//        Written on 2104246.165307.
//
#include <iso646.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>

typedef uint8_t u8;
typedef uint8_t byte;
typedef uint16_t half_word;
typedef uint16_t u16;
typedef uint32_t uint;
typedef uint32_t word;

static const int args_limit = 16;  	// number of arguments per instruction.
static const int defered_limit = 256; 	// number of branches allowed to be defered.
static const int ast_limit = 8192;   	// size of abstract syntax tree.
static const int uf2_limit = 4096; 	// number of half words in the output file.
static const int ctm_limit = 4096; 	// number of compiletime adressable uint32_t's 
					//       (ie, memory words) available for compiletime programming.
enum {long_branch = 16, load_literal = 17};
static const char* branch_conditions[16] = {
	"cond:eq;", "cond:ne;", "cond:cs;", "cond:cc;", "cond:mi;", "cond:pl;", "cond:vs;", "cond:vc;", 
	"cond:hi;", "cond:ls;", "cond:ge;", "cond:lt;", "cond:gt;", "cond:le;", "cond:al;", "cond:unused15;",
};
static const char* low_registers[8] = {
	"r:r0;", "r:r1;", "r:r2;", "r:r3;", "r:r4;", "r:r5;", "r:r6;", "r:r7;", 
};
static const char* high_registers[16] = {
	"R:r0;",  "R:r1;",  "R:r2;",  "R:r3;", "R:r4;",  "R:r5;",  "R:r6;",  "R:r7;", 
	"R:r8;",  "R:r9;",  "R:r10;", "R:r11;", "R:r12;", "R:r13;", "R:r14;", "R:r15;", 
};
static const char* ct_registers[32] = {
	"imm:c0;",  "imm:c1;",  "imm:c2;",  "imm:c3;", "imm:c4;",  "imm:c5;",  "imm:c6;",  "imm:c7;",
	"imm:c8;",  "imm:c9;",  "imm:c10;", "imm:c11;", "imm:c12;", "imm:c13;", "imm:c14;", "imm:c15;",
	"imm:c16;", "imm:c17;", "imm:c18;", "imm:c19;", "imm:c20;", "imm:c21;", "imm:c22;", "imm:c23;",
	"imm:c24;", "imm:c25;", "imm:c26;", "imm:c27;", "imm:c28;", "imm:c29;", "imm:c30;", "imm:c31;",
};

static inline void dumphex(u8* ibytes, size_t byte_count) {
	for (size_t i = 0; i < byte_count; i++) {
		if (!(i % 16)) printf("\n 0x%08zx: ", i);
		if (!(i % 8)) printf(" ");
		printf("%02x ", ibytes[i]);
	}
	printf("\n");
}

static inline void dump_byte_sequence(u8* ibytes, size_t byte_count) {
	for (size_t i = 0; i < byte_count; i++) {
		// if (!(i % 16)) printf("\n 0x%08zx: ", i);
		if (!(i % 8)) printf(" ");
		printf("%02X ", ibytes[i]);
	}
	printf("\n");
}

static inline int is(const char* string, const char* input, int start) { // , char terminator
	const char terminator = ';';
	int ii = start;
	while (input[ii] != terminator or *string != terminator) {
		if (input[ii] != *string) return 0;
		do ii++; while((u8)input[ii] < 33);
		do string++; while((u8)*string < 33);
	} return 1;
}

static inline uint find(int arg, const char* input, int* ast, const char** strings, uint count) {
	for (uint i = 0; i < count; i++) 
		if (is(strings[i], input, ast[ast[arg] + 2])) 
			return i; 
	// now, try to look for the variable, 
	// and return the register, which is the 

	// also, we actually need to store the... you know... 
	//    "find(..)" version of the register. not the argument index.

	abort();
}
static inline half_word ins3(uint op, uint Rd, uint Rn, uint Rm) {
	return (u16) (Rd | (Rn << 3) | (Rm << 6) | (op << 9));
}
static inline half_word ins2(uint op, uint Rdn, uint Rm) {
	return (u16) ((op << 8) | ((Rdn >> 3) << 7) | (Rm << 3) | (Rdn & 7));
}
static inline half_word ins2c(uint op, uint Rd, uint Rm, uint imm5) {
	return (u16) ((op << 11) | ((imm5 & 0x1f) << 6) | (Rm << 3) | Rd);
}
static inline half_word ins1c(uint op, uint Rd, uint imm8) {
	return (u16) ((op << 11) | (Rd << 8) | (imm8 & 0xff));
}

int main(const int argc, const char** argv) {
	if (argc < 2) exit(puts("usage: ./cmop <input S filename list>"));

	int index = 0, top = 0, begin = 0, done = 0, var = 0, length = 0, where = 0, best = 0;
	int* ast = malloc(ast_limit * sizeof(int));
	char* input = NULL;
	int* file_lengths = malloc((size_t) argc * sizeof(int));

	for (int i = 1; i < argc; i++) {
		struct stat file_data = {0};
		int file = open(argv[i], O_RDONLY);
		if (file < 0 or stat(argv[i], &file_data) < 0) { perror("open"); exit(1); }
		size_t file_length = (size_t) file_data.st_size;
		file_lengths[i - 1] = (int) file_length;
		char* contents = not file_length ? 0 : mmap(0, (size_t) file_length, PROT_READ, MAP_SHARED, file, 0);
		if (contents == MAP_FAILED) { perror("mmap"); exit(1); }
		close(file);
		input = realloc(input, (size_t) length + file_length);
		if (file_length) memcpy(input + length, contents, file_length);
		length += (int) file_length;
		munmap(input, file_length);
	}


	////TODO:  completely redo name-syntax of the language, to be:
 	// 
	//                    ((ret-type) my function (param-type) and (other-param-type))                 
	// 
    	//                                           ...much cleaner.

	if (not length) goto error;
i0: 	if (begin >= length) goto i3;
	if (input[begin] == 59) goto i3;
	if (input[begin] != 92) goto i2;
i1: 	begin++;
	if (begin >= length) goto i2;
	if ((u8)input[begin] < 33) goto i1;
i2: 	begin++;
	if (begin >= length) goto i0;
	if ((u8)input[begin] < 33) goto i2;
	goto i0;
i3: 	begin++;
	if (begin >= length) goto i4;
	if ((u8)input[begin] < 33) goto i3;	
i4:	if (top + 7 >= ast_limit) goto error;
	ast[top] = ast_limit;
	ast[top + 2] = 0;
	ast[top + 3] = 0;
	ast[top + 5] = 0;
	ast[top + 6] = begin;
	top += 4;
	best = begin;
_0:  	var = ast[top + 1];
	if (not var) goto _3;
	var = ast[var + 3];
_2: 	var++;
	if ((u8)input[var] < 33) goto _2;
	if (input[var] == 58) goto _16;
_3:	if (done >= length) goto _35;
	if (var >= length) goto _35;
	if (input[done] != 58) goto _3_;
	if (input[var] == 58) goto _8;
_3_: 	if (input[done] != input[var]) goto _35;
	if (input[done] != 92) goto _6;
_4: 	done++; 
	if ((u8)input[done] < 33) goto _4;
_5: 	var++;
	if ((u8)input[var] < 33) goto _5;
_6:	var++;
	if (var >= length) goto _7;
	if ((u8)input[var] < 33) goto _6;
_7: 	done++; 
	if (done >= length) goto _3;
	if ((u8)input[done] < 33) goto _7;
	goto _3;
_8:	done++;
	if ((u8)input[done] < 33) goto _8;
	begin = ast[top + 2];
_9:	if (input[done] == 59) goto _21;
	if (input[done] != 58) goto _10;
	if (top + 7 >= ast_limit) goto error;
	ast[top] = index;
	ast[top + 3] = done;
	ast[top + 5] = top;
	ast[top + 6] = begin;
	top += 4;
	index = 0;
	done = 0;
	goto _0;
_10:	if (input[done] != 92) goto _12;
_11: 	done++;
	if ((u8)input[done] < 33) goto _11;
_12:	if (begin >= length) goto _28;
	if (input[done] != input[begin]) goto _28;
_13: 	begin++;
	if (begin >= length) goto _14;
	if ((u8)input[begin] < 33) goto _13;
_14: 	done++;
	if ((u8)input[done] < 33) goto _14;
	if (begin <= best) goto _15; 
	best = begin;
	where = done;
_15:	goto _9;
_16:	index = ast_limit;
_17:	if (begin >= length) goto _20;
	if (input[begin] == 59) goto _20;
	if (input[begin] != 92) goto _19;
_18: 	begin++;
	if (begin >= length) goto _19;
	if ((u8)input[begin] < 33) goto _18;
_19: 	begin++;
	if (begin >= length) goto _19_;
	if ((u8)input[begin] < 33) goto _19;
_19_:	goto _17;
_20:	begin++;
	if (begin >= length) goto _20_;
	if ((u8)input[begin] < 33) goto _20;
_20_:	if (begin <= best) goto _21; 
	best = begin;
	where = done;
_21:	ast[top] = index;
	ast[top + 3] = done;
	var = ast[top + 1];
	if (not var) goto _27;
	if (top + 7 >= ast_limit) goto error;
	top += 4;
	ast[top + 1] = ast[var + 1];
	ast[top + 2] = begin;
	index = ast[var];
	done = ast[var + 3];
_22: 	done++;
	if ((u8)input[done] < 33) goto _22;
_23:	if (input[done] == 58) goto _26;
	if (input[done] != 92) goto _25;
_24: 	done++; 
	if ((u8)input[done] < 33) goto _24;
_25: 	done++;
	if ((u8)input[done] < 33) goto _25;
	goto _23;
_26:	done++;
	if ((u8)input[done] < 33) goto _26;
	goto _9;
_27:	if (begin == length) goto success;
_28:	if (index == ast_limit) goto _34;
	var = ast[index + 2];
_29:	if (input[var] == 58) goto _32;
	if (input[var] != 92) goto _31;
_30: 	var++;
	if ((u8)input[var] < 33) goto _30;
_31: 	var++;
	if ((u8)input[var] < 33) goto _31;
	goto _29;
_32:	var++;
	if ((u8)input[var] < 33) goto _32; 
	if (input[var] == 59) goto _33;
	if (input[var] == 58) goto _33;
	if (var == done) goto _35; 
	goto _32;
_33:	if (var == done) goto _35;
_34:	if (not top) goto error;
	top -= 4;
	index = ast[top];
	done = ast[top + 3];
	goto _28;
_35:	index += 4;
	if (index >= top) goto _34;
	if (ast[index] != ast_limit) goto _35;
	done = ast[index + 2];
	goto _0; 
success: top += 4;

	int 	this = 0, next = 0, arg_count = 0, 
		size = 0, defered_count = 0, skip = 0;
	char* I = input;
	int* args = malloc(args_limit * sizeof(int));
	int* defered = malloc(defered_limit * sizeof(int));
	word* ctr = calloc(32, 4);
	word* ctm = calloc(ctm_limit, 4);
	half_word* out = calloc(uf2_limit, 2);
	
code:	arg_count = 0;
	next = this;
	if (this >= top) goto out;
	if (ast[this] == ast_limit) goto move;
	if (input[ast[this + 3]] != 59) goto move;
nxc: 	index = ast[next];
	if (index == ast_limit) goto first;
	done = ast[next + 3];
	var = ast[index + 2];
fail:	if (input[var] == 58) goto more;
	if (input[var] != 92) goto jj;
kk: 	var++;
	if ((u8)input[var] < 33) goto kk;
jj: 	var++;
	if ((u8)input[var] < 33) goto jj;
	goto fail;
more:	var++;
	if ((u8)input[var] < 33) goto more;
	if (input[var] == 59) goto check;
	if (input[var] == 58) goto check;
	goto more;
check:	if (var == done) goto first;
	args[arg_count++] = next - 4;
	next = ast[next - 3];
	goto nxc;
first:;
	int s = ast[ast[this] + 2], a0 = 0, a1 = 0, a2 = 0; 
	uint cond = 0;

	if (arg_count >= 1) 	a0 = args[arg_count - 1];
	if (arg_count >= 2) 	a1 = args[arg_count - 2];
	if (arg_count >= 3) 	a2 = args[arg_count - 3];
#	define c0 	find(a0, I, ast, ct_registers, 32)
#	define c1 	find(a1, I, ast, ct_registers, 32)
#	define c2 	find(a2, I, ast, ct_registers, 32)
#	define r0 	find(a0, I, ast, low_registers, 8)
#	define r1 	find(a1, I, ast, low_registers, 8)
#	define r2 	find(a2, I, ast, low_registers, 8)
#	define R0 	find(a0, I, ast, high_registers, 16)
#	define R1 	find(a1, I, ast, high_registers, 16)

	if (skip) {
		if (is("unit:at:ctl:\\::unit:;", I, s) and ast[a0] == skip) {
			ast[ast[a0] + 3] = a1; skip = 0; this = a1; goto code;
		}
		goto move;
	}
/**/	     if (is("unit:nop;", I, s))   		out[size++] = 0xBF00;
/**/	else if (is("unit:yield;", I, s)) 		out[size++] = 0xBF10;
/**/	else if (is("unit:wfe;", I, s))   		out[size++] = 0xBF20;
/**/	else if (is("unit:wfi;", I, s))   		out[size++] = 0xBF30;
/**/	else if (is("unit:sev;", I, s))   		out[size++] = 0xBF40;
/**/	else if (is("unit:emit:imm:;", I, s)) 		out[size++] = (half_word) ctr[c0];
/**/	else if (is("unit:align word;", I, s)) 		{ if (size & 1) out[size++] = 0x0000; }
/**/	else if (is("unit:emit word:imm:;", I, s)) 	{ out[size++] = (half_word) ctr[c0]; out[size++] = (half_word) (ctr[c0] >> 16); }

/**/	else if (is("unit:adcs:r:,:r:;", I, s)) 	out[size++] = ins3(0x20, r0, r1, 5);
/**/	else if (is("unit:adds:r:,:r:,:r:;", I, s)) 	out[size++] = ins3(0x0C, r0, r1, r2);
/**/	else if (is("unit:add:R:,:R:;", I, s)) 		out[size++] = ins2(0x44, R0, R1);
/**/	else if (is("unit:subs:r:,:r:,:r:;", I, s)) 	out[size++] = ins3(0x0D, r0, r1, r2);
/**/	else if (is("unit:cmp:r:,:r:;", I, s)) 		out[size++] = ins3(0x21, r0, r1, 2);
/**/	else if (is("unit:cmn:r:,:r:;", I, s)) 		out[size++] = ins3(0x21, r0, r1, 3);
/**/	else if (is("unit:cmp:r:,:imm:;", I, s)) 	out[size++] = ins1c(0x05, r0, ctr[c1]);
/**/	else if (is("unit:muls:r:,:r:;", I, s)) 	out[size++] = ins3(0x21, r0, r1, 5);

/**/	else if (is("unit:movs:r:,:imm:;", I, s)) 	out[size++] = ins1c(0x04, r0, ctr[c1]);
/**/	else if (is("unit:movs:r:,:r:;", I, s)) 	out[size++] = ins3(0x00, r0, r1, 0);
/**/	else if (is("unit:mov:R:,:R:;", I, s)) 		out[size++] = ins2(0x46, R0, R1);
/**/	else if (is("unit:mvns:r:,:r:;", I, s)) 	out[size++] = ins3(0x21, r0, r1, 7);

/**/	else if (is("unit:lsls:r:,:r:,:imm:;", I,s)) 	out[size++] = ins2c(0x00, r0, r1, ctr[c2]);
/**/	else if (is("unit:lsls:r:,:r:;", I, s)) 	out[size++] = ins3(0x20, r0, r1, 2);
	
/**/	else if (is("unit:lsrs:r:,:r:,:imm:;", I,s)) 	out[size++] = ins2c(0x01, r0, r1, ctr[c2]);
/**/	else if (is("unit:lsrs:r:,:r:;", I, s)) 	out[size++] = ins3(0x20, r0, r1, 3);
/**/	else if (is("unit:asrs:r:,:r:,:imm:;", I,s)) 	out[size++] = ins2c(0x02, r0, r1, ctr[c2]);
/**/	else if (is("unit:asrs:r:,:r:;", I, s)) 	out[size++] = ins3(0x20, r0, r1, 4);

/**/	else if (is("unit:bics:r:,:r:;", I, s)) 	out[size++] = ins3(0x21, r0, r1, 6);
/**/	else if (is("unit:tst:r:,:r:;", I, s)) 		out[size++] = ins3(0x21, r0, r1, 0);
/**/	else if (is("unit:ands:r:,:r:;", I, s)) 	out[size++] = ins3(0x20, r0, r1, 0);
/**/	else if (is("unit:eors:r:,:r:;", I, s)) 	out[size++] = ins3(0x20, r0, r1, 1);
/**/	else if (is("unit:orrs:r:,:r:;", I, s)) 	out[size++] = ins3(0x21, r0, r1, 4);

/**/	else if (is("unit:ldr:r:,[:r:,:r:];", I,s)) 	out[size++] = ins3(0x2C, r0, r1, r2);
/**/	else if (is("unit:ldr:r:,[:r:,:imm:];", I,s)) 	out[size++] = ins2c(0x0D, r0, r1, ctr[c2]); // does multiply of c2 by 4 implicitly.
/**/	else if (is("unit:ldr:r:,[sp,:imm:];", I,s)) 	out[size++] = ins1c(0x13, r0, ctr[c1]); // does multiply of c2 by 4 implicitly.
/**/	else if (is("unit:ldr:r:,[pc,:imm:];", I,s)) 	out[size++] = ins1c(0x09, r0, ctr[c1]); // does multiply of c2 by 4 implicitly.
	
/**/	else if (is("unit:str:r:,[:r:,:r:];", I, s)) 	out[size++] = ins3(0x28, r0, r1, r2);  
/**/	else if (is("unit:str:r:,[:r:,:imm:];", I,s)) 	out[size++] = ins2c(0x0C, r0, r1, ctr[c2]);  // does multiply of c2 by 4 implicitly.
/**/	else if (is("unit:str:r:,[sp,:imm:];", I,s)) 	out[size++] = ins1c(0x12, r0, ctr[c1]);  // does multiply of c2 by 4 implicitly.

/**/	else if (is("unit:ldr:r:,:label:;", I,s)) 	{ cond = load_literal; goto generate_branch; } // note: only can go forwards!! 
/**/	else if (is("unit:b:label:;", I, s)) 		{ cond = long_branch; a1 = a0; goto generate_branch; }
/**/	else if (is("unit:b:cond::label:;", I, s)) {

		/// todo: impl a 32bit ins, and tesst a branch to that.
		/// ...it might not work.

		cond = find(a0, input, ast, branch_conditions, 16);
		generate_branch:; word offset = 0;
		int loc = ast[a1] + 3, target = ast[loc];
		
		if (not target) {
			defered[defered_count++] = loc;
			defered[defered_count++] = size;
			defered[defered_count++] = (int) cond;
		} else offset = (word)(target - 2) - (word)(size + 1);
		
		if (cond == load_literal) out[size++] = ins1c(0x09, r0, offset >> 1);
		else if (cond == long_branch) out[size++] = 0xE000 | (u16)(0x07ff & offset);
		else if (cond < long_branch) out[size++] = 0xD000 | (u16)(cond << 8) | (u16)(0x00ff & offset);
		else abort();

	} else if (is("unit:signed if :imm: is less than :imm:,:ctl:;", I, s)) {
		if ((signed int) ctr[c0] < (signed int) ctr[c1]) goto ct_branch;
	
	} else if (is("unit:if :imm: is less than :imm:,:ctl:;", I, s)) {
		if (ctr[c0] < ctr[c1]) {
			ct_branch: if (ast[ast[a2] + 3]) { this = ast[ast[a2] + 3]; goto code; }
			skip = ast[a2];
		}
	}
	else if (is("unit::label:\\::unit:;", I, s)) 		ast[ast[a0] + 3] = size;
	// else if (is("unit:ctset:ctvar:=:imm:;", I, s)) 		ast[ast[a0] + 3] = a1;
	// else if (is("unit:rtset:rtvar:=:r:;", I, s)) 		ast[ast[a0] + 3] = a1;
	else if (is("unit:at:ctl:\\::unit:;", I, s)) 		ast[ast[a0] + 3] = a1;
	else if (is("unit:address:imm:;", I, s)) 		ctr[c0] = (word) size;
	else if (is("unit:increment:imm:;", I, s)) 		ctr[c0]++;
	else if (is("unit:decrement:imm:;", I, s)) 		ctr[c0]--;
	else if (is("unit:zero:imm:;", I, s)) 			ctr[c0] = 0;
	else if (is("unit:complement:imm:,:imm:;", I, s)) 	ctr[c0] = ~ctr[c1];
	else if (is("unit:copy:imm:,:imm:;", I, s)) 		ctr[c0] = ctr[c1];
	else if (is("unit:add:imm:,:imm:,:imm:;", I, s)) 	ctr[c0] = ctr[c1] + ctr[c2];
	else if (is("unit:subtract:imm:,:imm:,:imm:;", I, s)) 	ctr[c0] = ctr[c1] - ctr[c2];
	else if (is("unit:multiply:imm:,:imm:,:imm:;", I, s)) 	ctr[c0] = ctr[c1] * ctr[c2];
	else if (is("unit:xor:imm:,:imm:,:imm:;", I, s)) 	ctr[c0] = ctr[c1] ^ ctr[c2];
	else if (is("unit:and:imm:,:imm:,:imm:;", I, s)) 	ctr[c0] = ctr[c1] & ctr[c2];
	else if (is("unit:or:imm:,:imm:,:imm:;", I, s)) 	ctr[c0] = ctr[c1] | ctr[c2];
	else if (is("unit:right:imm:,:imm:,:imm:;", I, s)) 	ctr[c0] = ctr[c1] >> ctr[c2];
	else if (is("unit:left:imm:,:imm:,:imm:;", I, s)) 	ctr[c0] = ctr[c1] << ctr[c2];
	else if (is("unit:store[:imm:],:imm:;", I, s)) 		ctm[ctr[c0]] = ctr[c1];
	else if (is("unit:load:imm:,[:imm:];", I, s)) 		ctr[c0] = ctm[ctr[c1]];

move: 	this += 4;
	goto code;
out:;
	if (skip) { printf("error: ctl unbound\n"); abort();} 

	for (int i = 0; i < defered_count; i += 3) {
		int target = ast[defered[i]], dest = defered[i + 1]; cond = (uint) defered[i + 2];
		if (not target) { where = best = ast[(defered[i] - 3) + 2]; top = 1; goto error; }
		word offset = (word)(target - 2) - (word)(dest + 1);
		if (cond == load_literal) offset >>= 1;
		out[dest] |= (u16)((cond == long_branch ? 0x07ff : 0x00ff) & offset); 
	}

	printf("DEBUG: ctr:\n{\n");
	for (int i = 0; i < 32; i++) 
		if (ctr[i]) printf("\tc%d = %12u | 0x%08x\n", i, ctr[i], ctr[i]);
	printf("}\n");

	printf("DEBUG: ctm:\n{\n");
	for (int i = 0; i < ctm_limit; i++) 
		if (ctm[i]) printf("\t[%d] = %12u | 0x%08x\n", i, ctm[i], ctm[i]);
	printf("}\n");

	printf("\nprinting byte sequence:\n------------------------\n");
	dump_byte_sequence((void*) out, (size_t)size * 2);

	printf("\ndebugging bytes:\n------------------------\n");
	dumphex((void*) out, (size_t)size * 2);

	printf("debug: outputting %d bytes to uf2 file...\n", size * 2);

	int out_file = open("out.uf2", O_WRONLY | O_CREAT | O_TRUNC); 
	if (out_file < 0) { 
		perror("open"); 
		exit(1); 
	}

	word blockcount = 0; 
	while (blockcount * 256 < (word)(size << 1)) blockcount++;
	
	printf("writing %u UF2 blocks...\n", blockcount);
	
	byte data[476];
	word head[8] = {
		0x0A324655, 0x9E5D5157, 
		0x00002000, 0x10000000,
		0x00000100, 0x00000000, 
		blockcount, 0xe48bff56,
	};
	for (word i = 0; i < (word) size; i += 128, head[5]++, head[3] += 256) {
		memset(data, 0, 476);
   		memcpy(data, out + i, 256);

		printf("BLOCK %u / %u :  writing header: \n", head[5], head[6]);
		dumphex((void*) head, 32);
		printf("BLOCK %u / %u :  with payload: \n", head[5], head[6]);
		dumphex((void*) data, 256);

		write(out_file, head, 32);
		write(out_file, data, 476);
		write(out_file, (int[]) {0x0AB16F30}, 4);
	}
	printf("debug: wrote %u blocks to file %s\n", blockcount, "out.uf2");

	close(out_file);
	goto clean_up;

error:; 
	int 	e_file = 0, e_at = 0, e_offset = 0, e_line = 1, e_column = 1, 
		w_file = 0, w_at = 0, w_offset = 0, w_line = 1, w_column = 1;
	
	while (e_at < best and e_at < length) {
		if (e_offset >= file_lengths[e_file]) { e_file++; e_offset = 0; e_line = 1; e_column = 1; }
		if (input[e_at] != 10) { e_column++; } 
		else { e_line++; e_column = 1; }
		e_offset++; e_at++;
	}
	while (w_at < where and w_at < length) {
		if (w_offset >= file_lengths[w_file]) { w_file++; w_offset = 0; w_line = 1; w_column = 1; }
		if (input[w_at] != 10) { w_column++; } 
		else { w_line++; w_column = 1; }
		w_offset++; w_at++;
	}

	fprintf(stderr, "%u %s %u %u %u %s %u %u %u\n", top, 
		argv[e_file + 1], e_line, e_column, e_offset, 
		argv[w_file + 1], w_line, w_column, w_offset
	);
clean_up:
	free(input);
	free(ast);

}



//todo:   add variable alignment to the assembler..?

// // else if (is("unit:variable alignment :imm:;", I, s)) 		{ while (size & (1 << )) out[size++] = 0x0000; }

// todo:  implement a rotate right.

