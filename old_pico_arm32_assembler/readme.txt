A Raspberry Pi Pico Assembler.
==============================

Overview:
---------

	An assembler for the RP2040, a ARMv6-M Cortex-M0+ CPU. found in the Raspberry Pi Pico micro-controller.

	Not intended for production use. just a fun hobby project, mainly for my own uses.

	Written by Daniel Warren Riaz Rehman.


Some noteworthy remarks:
------------------------

	- uses the BTUCSR algorithm for its syntax.

	- allows for assembling multiple files into a single output.

	- features a turing-complete compile-time programming system for emitting arbitrary executable/constant data.

	- outputs a UF2 file, to be written to flash only. more output options might be added in the future.

	- source is currently under 500 lines of C code.

	- planning to have macros, and some abstractions eventually.

	- currently still in development!


Hope you like it!

- dwrr