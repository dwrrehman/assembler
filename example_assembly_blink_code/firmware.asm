; written on 1202409032.204656 dwrr
; solar fram personal computer with LED display

#include "msp430fr5994.h"
            .global _main
            .global __STACK_END
            .sect   .stack
            .text
_main:
RESET:	mov.w   #__STACK_END,SP
	mov.w   #WDTPW+WDTHOLD,&WDTCTL
	bic.b   #BIT6,&P2OUT
	bis.b   #BIT6,&P2DIR
	bic.w   #LOCKLPM5,&PM5CTL0

Mainloop    xor.b   #BIT6,&P2OUT
Wait        mov.w   #50000,R15
L1          dec.w   R15
            jnz     L1
            jmp     Mainloop
            .sect   ".reset"
            .short  RESET
            .end


