; written on 1202409032.204656 dwrr
; solar fram personal computer with LED display

;  MSP430FR69x Demo - Toggle P1.0 using software
;
;  Description: Toggle P1.0 using software.
;  ACLK = n/a, MCLK = SMCLK = TACLK = default DCO = ~625 KHz
;
;           MSP430FR6989
;         ---------------
;     /|\|               |
;      | |               |
;      --|RST            |
;        |               |
;        |           P1.0|-->LED


#include "msp430fr5994.h"

            .def    RESET
            .global _main
            .global __STACK_END
            .sect   .stack 

            .text                           ; Assemble to Flash memory
            .retain                         ; Ensure current section gets linked
            .retainrefs
_main
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW+WDTHOLD,&WDTCTL  ; Stop WDT
SetupP1     bic.b   #BIT6,&P2OUT            ; Clear P2.6 output latch for a defined power-on state
            bis.b   #BIT6,&P2DIR            ; Set P2.6 to output direction
UnlockGPIO  bic.w   #LOCKLPM5,&PM5CTL0      ; Disable the GPIO power-on default

Mainloop    xor.b   #BIT6,&P2OUT            ; Toggle P2.6
Wait        mov.w   #50000,R15              ; Delay to R15
L1          dec.w   R15                     ; Decrement R15
            jnz     L1                      ; Delay over?
            jmp     Mainloop                ; Again

            .sect   ".reset"
            .short  RESET
            .end


